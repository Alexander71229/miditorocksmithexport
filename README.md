# MidiToRocksmithExport

Programa de Java para generar arreglos de Rocksmith a partir de archivos midi.\
El tab se genera automáticamente usando la descripción del instrumento ingresado en el archivo de entrada.\
Por las limitaciones propias de rocksmith, los instrumentos deben tener 4 o 6 cuerdas.

Java program to generate Rocksmith arrangements from midi files.\
The tab is automatically generated using the instrument description entered in the input file.\
Due to rocksmith's own limitations, the instruments must have 4 or 6 strings.

## Requirements
Java JDK 8

## Usage

java -jar miditorocksmith.jar File

File: Absolute or relative path to the description file or folder containing the description files.

Examples:\
java -jar miditorocksmith.jar song.txt\
java -jar miditorocksmith.jar FolderSongs

## Description file
The description file is a plain text file with the midi file information and the description of the instruments for which you want to create arrangements.

Format file:\
Absolute or relative path where the arrangements will be saved, example: ./Export/Proyecto/wcsp/\
"midi:" followed by absolute or relative path to the midi file, example: midi:Proyectos/wcsp.mid\
"title:" followed by the song title, example: title:wcsp\
"artistName:" followed by the name of the artist, example: artistName:Gregor Heimz\
"albumName:" followed by the name of the album, example: albumName:Album Zero\
"albumYear:" followed by the year, example: albumYear:2022\
Any of the following values, which indicate the role of the instrument in Rocksmith: "lead", "lead2", "rhythm", "rhythm2", "bass", "bass2"\
"frets:" followed by the number of frets that the instrument has, example: frets:21\
Optional sequence of numbers indicating the relative tuning of the instrument relative to the guitar or bass (indicated by the role).\
"open:" followed by "true" or "false" (Optional. Indicates if open strings will be used), example: open:false\
"channel:" or "track" followed by a number will be used to indicate which channel or track of the midi file corresponds to the instrument of the arrangement, example: channel:2\
"anchor:" followed by the number of frets needed to repeat a note on the next string (examples: 5 bass, 5 guitar, 7 violin, etc), example: anchor:5.\
"tanchor:" followed by a number representing the minimum time in milliseconds that must elapse between notes, before changing the anchor (shifting), example: tanchor:1000\
"transpose:" followed by transposition in semitones, example: transpose:-12\
"tonebase:" followed by the name of the tone base, example: tonebase:distortion_lead


## Sample file
```
./Export/Proyecto/wcsp/
midi:Proyectos/wcsp.mid
title:wcsp
artistName:wcsp
albumName:wcsp
albumYear:2022
lead
frets:21
open:false
channel:2
anchor:5
tanchor:1000
transpose:-12
tonebase:wcsp_lead
bass2
frets:10
27
29
31
33
open:true
channel:0
anchor:7
tanchor:1000
transpose:0
tonebase:wcsp_lead
```

## Explanation of the above file
The file has two arrangement descriptions (lead, bass2), so it will generate two arrangement files in XML format (in the relative path ./Export/Proyecto/wcsp/).\
In the bass2 arrangement, a violin is being described, like a specially tuned bass.\
With that tuning the arrangement bass2 would be suitable for a violin.
